#ifndef _PIPE_STREAMED_H

#include <iostream>
#include <ostream>
#include <istream>

using namespace std;

class PipeStreamed {
 private:
  int pipe[2];
  bool closeIn;
  bool closeOut;
  ostream* out;
  istream* in;
public:
  PipeStreamed();
  ~PipeStreamed();
  ostream& getDupOStream(int fildes);
  istream& getDupIStream(int fildes);
  ostream& getOStream();
  istream& getIStream();
  void closeOStream();
  void closeIStream();
};
#endif
